#!/bin/sh
set echo on

export CURRENT_APP_HOME=$WORKSPACE/loa-application

cd $CURRENT_APP_HOME
echo build "loa-application"
mvn -q clean install -DskipTests
mkdir -p target/dependency 
cd target/dependency
jar -xf $CURRENT_APP_HOME/target/app.jar
cd ../..
docker build -t loa-application:latest .
cd $SLR_BUILD_HOME



