# loa-application

## Register new User
- Here you will [short screencast](https://fair.tube/w/iQw7wZXgWkTLduptWeFLPG) that shows you how to register a new user.  
  - You can also click on "SignIn" on the [start page](https://loa-app.test.opensourceecology.de/actuator/info) of the application

## Import CSV File to your outbox
After starting the application and signIn, you are able to click on the `CSV import menu item`:

![CSV_Import_Menu_Item](CSV_Import_Menu_Item.png "CSV import menu item")

then a upload dialog appears:

![upload dialog](uploadDialog.png "upload dialog")

The csv to be imported must have the following columns in the following order. No columns must be missing, if there is no value for a column, it must be left empty. [Samle csv file](BioContatti_LOA_Bolsena.csv)

1. PublicationLoa_version
1. PublicationLoa_copyrightNotice
1. PublicationLoa_creativeWorkStatus
1. PublicationLoa_dateCreated
1. PublicationLoa_dateModified
1. PublicationLoa_license
1. PublicationLoa_keywords
1. PublicationLoa_identifier
1. PublicationLoa_description
1. OrgansationLoa_legalName
1. OrgansationLoa_name
1. OrgansationLoa_url
1. PlaceLoa_latitude
1. PlaceLoa_longitude
1. PostalAddressLoa_postalCode
1. PostalAddressLoa_addressLocality
1. PostalAddressLoa_addressRegion
1. PostalAddressLoa_addressCountry
1. PostalAddressLoa_streetAddress
1. ContactPointLoa_email
1. ContactPointLoa_name
1. ContactPointLoa_telephone

### Validate imported activities
After clicking on the outbox menue item: 

![outbox menu item](doc/outbox_Import_Menu_Item.png "outbox menu item")

you get a table with the activities in the outbox:

![outbox](doc/outbox.png "outbox")

The `subject` column links to a simple representation of the activity on the rdf-pub server.


