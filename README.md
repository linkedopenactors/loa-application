# loa-application

This is the second version of the linked open actors application. It is the successor of https://gitlab.com/linkedopenactors/loa-suite/-/tree/develop/loa-app/loa-app-spring-boot


## How-To
[you can find the manual here](doc/Readme.md) 

## misc
See https://developer.okta.com/blog/2022/03/24/thymeleaf-security


