package org.linkedopenactors.code.loaapp;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.springframework.stereotype.Component;

@Component
class RdfTupleFactoryDefault implements RdfTupleFactory {

	public RdfTuple create(String nodeId, String parent, boolean objectIsIRI, String subject, String object, String predicate) {
		Boolean iri = false;
		Boolean rdfPubContext = false;
		String encodedObjectUrl = null;			
		if(objectIsIRI) {
			iri = true;
			URL objectURL = null;
			URL namespaceURL = null;
			
			try {
				// TODO cleanup !!!
				namespaceURL = new URL(subject);
				objectURL = new URL(object);
				String objectId = objectURL.getProtocol() + "_" + objectURL.getHost()  + "_" + objectURL.getPort();
				String namespaceId = namespaceURL.getProtocol() + "_" + namespaceURL.getHost()  + "_" + namespaceURL.getPort();
				rdfPubContext = objectId.equals(namespaceId);
				if(rdfPubContext) {
					encodedObjectUrl = URLEncoder.encode(object);
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}			
		return new RdfTuple(nodeId, parent, subject, predicate, object, encodedObjectUrl, iri, rdfPubContext);
	}
}
