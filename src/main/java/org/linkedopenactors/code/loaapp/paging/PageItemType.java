package org.linkedopenactors.code.loaapp.paging;

public enum PageItemType {

    DOTS,
    PAGE

}