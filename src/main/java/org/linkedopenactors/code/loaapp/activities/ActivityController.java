package org.linkedopenactors.code.loaapp.activities;

import java.util.UUID;
import java.util.function.Function;

import javax.validation.Valid;

import org.eclipse.rdf4j.model.IRI;
import org.linkedopenactors.code.loaapp.paging.Paged;
import org.linkedopenactors.code.loaapp.service.RdfPubAdapter;
import org.linkedopenactors.rdfpub.client.RdfPubClientSession;
import org.linkedopenactors.rdfpub.client.RdfPubClientSessionFactory;
import org.linkedopenactors.rdfpub.client.RdfPubClientWebFlux;
import org.linkedopenactors.rdfpub.client.RdfPubClientWebFluxDefault;
import org.linkedopenactors.rdfpub.domain.commonsrdf.Activity;
import org.linkedopenactors.rdfpub.domain.commonsrdf.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.commonsrdf.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.result.view.Rendering;
import org.springframework.web.server.WebSession;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Controller
@Slf4j
public class ActivityController {

	@Autowired
	private RdfPubClientSessionFactory rdfPubClientSessionFactory; 
	
	@Autowired
	private WebClient webClient;

	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;

	@Autowired
	private RdfPubAdapter rdfPubAdapter;

	@Autowired
	private ActivityService activityService;

	@Value("${loa_app.rdfPubServerUrl}")	
	private String rdfPubServerUrl;

	
    @GetMapping("/user/outbox")
    public String outbox(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
            @RequestParam(value = "size", required = false, defaultValue = "5") int size, Model model
            ,Authentication authentication
            , @RegisteredOAuth2AuthorizedClient("loa-application") OAuth2AuthorizedClient authorizedClient, WebSession session) {
    	
    	RdfPubClientWebFlux rdfPubClientWebFlux = new RdfPubClientWebFluxDefault(rdfPubServerUrl, webClient, authentication.getName(), activityPubObjectFactory);
    	Mono<RdfPubClientSession> rdfPubClientSession = rdfPubClientSessionFactory.getSession(rdfPubClientWebFlux, session);
    	
    	String token = authorizedClient.getAccessToken().getTokenValue();
    	log.debug("->activityService.getPageRdfPub");
    	
    	
    	Mono<Actor> currentLoggedInActor = rdfPubClientSession.flatMap(s->s.getCurrentLoggedInActor());
    	
    	
//    	Mono<Paged<Activity>> page = 
    	Mono<Paged<Activity>> page =
    			currentLoggedInActor
    			.map(actor->activityService.getPageRdfPub(pageNumber, size, actor, token, AvailableCollection.outbox))
    			.flatMap(Function.identity());
    	
    	log.debug("<-activityService.getPageRdfPub");
		model.addAttribute("posts", page);
		model.addAttribute("moreUrl", "outbox");
		model.addAttribute("collectionName", "Outbox");
        return "showCollection";
    }
    
    @GetMapping("/user/inbox")
    public String inbox(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
            @RequestParam(value = "size", required = false, defaultValue = "5") int size, Model model
            ,Authentication authentication
            , @RegisteredOAuth2AuthorizedClient("loa-application") OAuth2AuthorizedClient authorizedClient, WebSession session) {
    	RdfPubClientWebFlux rdfPubClientWebFlux = new RdfPubClientWebFluxDefault(rdfPubServerUrl, webClient, authentication.getName(), activityPubObjectFactory);
    	Mono<RdfPubClientSession> rdfPubClientSession = rdfPubClientSessionFactory.getSession(rdfPubClientWebFlux, session);

    	String token = authorizedClient.getAccessToken().getTokenValue();
    	log.debug("->activityService.getPageRdfPub");
    	
    	Mono<Paged<Activity>> page =
//    	Mono<Object> page =
    			rdfPubClientSession.flatMap(s->s.getCurrentLoggedInActor())
        		.flatMap(actor->activityService.getPageRdfPub(pageNumber, size, actor, token, AvailableCollection.inbox));

    	log.debug("<-activityService.getPageRdfPub");
		model.addAttribute("posts", page);
		model.addAttribute("moreUrl", "inbox");		
		model.addAttribute("resolveUrl", rdfPubServerUrl + "resolve?url=");
		model.addAttribute("collectionName", "Inbox");
        return "showCollection";
    }

//	@GetMapping(value = "/resolve")
////	@ResponseBody
//	public Mono<String> resolve(@RequestParam("url") String url, Authentication authentication) {
//		RdfPubClientWebFlux rdfPubClientWebFlux = new RdfPubClientWebFluxDefault(rdfPubServerUrl, webClient, authentication.getName(), activityPubObjectFactory);
//		return rdfPubClientWebFlux.read(Values.iri(url));
////        return "showCollection";
//    }

    @GetMapping("/user/public")
    public String asPublic(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
            @RequestParam(value = "size", required = false, defaultValue = "5") int size, Model model
            ) {
    	Mono<org.eclipse.rdf4j.model.Model> serverProfile = rdfPubAdapter.getServerProfile().log();
    	Mono<IRI> subject = serverProfile.map(this::extractSubject);
    	
    	log.debug("->activityService.getPageRdfPub");
//    	Mono<Paged<Activity>> page = 
    	Mono<Object> page =
    			subject.flatMap(subj->activityService.getPublicPageRdfPub(pageNumber, size));
    	log.debug("<-activityService.getPageRdfPub");
		model.addAttribute("posts", page);
		model.addAttribute("moreUrl", "public");
		model.addAttribute("collectionName", "Public");
        return "showCollection";
    }

    private IRI extractSubject(org.eclipse.rdf4j.model.Model rdfPubServerProfile) {
    	org.eclipse.rdf4j.model.Model filtered = rdfPubServerProfile.filter(null, null, null);
    	IRI e = filtered.stream()
    	.findFirst()
    	.map(stmt->stmt.getSubject())
    	.map(IRI.class::cast)
    	.orElseThrow(()->new IllegalStateException("no subject found in serverProfile!"));
    	return e;
    }
    
    @GetMapping("/newActivityForm")
    public Mono<Rendering> postIt() {    	
        NewActivityForm newActivityForm = new NewActivityForm();
        
        newActivityForm.setMessage("""
        		@prefix as: <https://www.w3.org/ns/activitystreams#> .
				@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
				
				<https://social.example/alyssa/posts/a29a6843-9feb-4c74-a7f7-081b9c9201d3>
				  a as:Create ;
				  <https://schema.org/version> "1"^^<http://www.w3.org/2001/XMLSchema#long>;
				  <https://schema.org/identifier> "%s" ;
				  as:actor <http://localhost:8081/actor/1bdbbb62-3bb1-4a9c-8f7d-4a56e1696f4e> ;
				  as:object <https://social.example/alyssa/posts/49e2d03d-b53a-4c4c-a95c-94a6abf45a19> ;
				  as:name "Some Note Create Activity" ;
				  as:to <https://chatty.example/ben/> .
				
				<https://social.example/alyssa/posts/49e2d03d-b53a-4c4c-a95c-94a6abf45a19>
				  a as:Note ;
				  as:attributedTo <https://social.example/alyssa/> ;
				  <https://schema.org/version> "1"^^<http://www.w3.org/2001/XMLSchema#long>;
				  <https://schema.org/identifier> "%s" ;
				  as:name "Some Note" ;
				  as:content "Say, did you finish reading that book I lent you?"^^xsd:string ;
				  as:to <https://chatty.example/ben/> .
        		""".formatted(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        return Mono.just(Rendering.view("activity/newActivityForm").modelAttribute("newActivityForm", newActivityForm).build());
    }

    @RequestMapping(value = "/newActivityForm", method = RequestMethod.POST)
	public Mono<Rendering> processAdd(@Valid @ModelAttribute("newActivityForm") NewActivityForm postForm,
			BindingResult bindingResult, Authentication authentication,
			@RegisteredOAuth2AuthorizedClient("loa-application") OAuth2AuthorizedClient authorizedClient) {
        if (bindingResult.hasErrors()) {
            return Mono.just(Rendering.view("newActivityForm").build());
        }
        String token = authorizedClient.getAccessToken().getTokenValue();
        Mono<IRI> activity = activityService.postActivity(postForm.getMessage(), authentication.getName(), token);
        
    	return Mono.just(Rendering.view("activity/newActivityCreated").modelAttribute("activity", activity).build());
    }
}
