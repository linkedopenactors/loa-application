package org.linkedopenactors.code.loaapp.activities;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.linkedopenactors.code.loaapp.paging.Paged;
import org.linkedopenactors.code.loaapp.paging.Paging;
import org.linkedopenactors.rdfpub.client.RdfPubClientAnonymousWebFlux;
import org.linkedopenactors.rdfpub.client.RdfPubClientWebFlux;
import org.linkedopenactors.rdfpub.client.RdfPubClientWebFluxAnonymousDefault;
import org.linkedopenactors.rdfpub.client.RdfPubClientWebFluxDefault;
import org.linkedopenactors.rdfpub.domain.commonsrdf.Activity;
import org.linkedopenactors.rdfpub.domain.commonsrdf.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.commonsrdf.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.Vocabularies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Slf4j
class ActivityService {

	@org.springframework.beans.factory.annotation.Value("${loa_app.rdfPubServerUrl}")	
	private String rdfPubServerUrl;

	@Autowired
	private WebClient webClient;

	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;
	
	@Autowired
	private Vocabularies vocabularies;

    public Mono<Paged<Activity>> getPublicPageRdfPub(int pageNumber, int size) {
		return getPageRdfPub(pageNumber, size, AS.Public.stringValue(), vocabularies.getActivityStreams().Public(), null, AvailableCollection.asPublic);
    }

    public Mono<Paged<Activity>> getPageRdfPub(int pageNumber, int size, Actor actor, String authToken, AvailableCollection collection) {
    	return getPageRdfPub(pageNumber, size, actor.getOauth2IssuerUserId().orElse(null), actor.getSubject(), authToken, collection);
    }
    
    private Mono<Paged<Activity>> getPageRdfPub(int pageNumber, int size, String userId, org.apache.commons.rdf.api.IRI actorId, String authToken, AvailableCollection collection) {
    	Flux<Activity> fluxActivities = getPage(pageNumber, size, userId, actorId, authToken, collection);
    	Mono<Long> countActivitiesMono = fluxActivities.count();
//    	Mono<Long> countActivitiesMono = countActivities(userId, authToken, collection);
   	
    	Mono<Paged<Activity>> res = fluxActivities.collectList()
    		.zipWith(countActivitiesMono)
        	.map(tuple2 -> {
        		List<Activity> activities = tuple2.getT1();
        		Long count = tuple2.getT2();
        		
        		PageRequest pageRequest = PageRequest.of(pageNumber - 1, size);//, new Sort());        		
     		   	Page<Activity> postPage = new PageImpl<Activity>(activities, pageRequest, count);
     		   	return new Paged<Activity>(postPage, Paging.of(postPage.getTotalPages(), pageNumber, size));
        });
		return res;
    }
    
    public Mono<IRI> postActivity(String modelAsString, String userId, String authToken) {
    	
    	StringReader sr = new StringReader(modelAsString);
    	try {
			Model model = Rio.parse(sr, RDFFormat.TURTLE);
	    	RdfPubClientWebFlux rdfPubClientWebFlux = getRdfPubClientWebFlux(userId);
	    	Mono<IRI> activity = rdfPubClientWebFlux.postActivity(model, authToken);
	    	return activity; 
		} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Mono.empty();
		}
    }
    
//   	
    
    private Flux<org.linkedopenactors.rdfpub.domain.commonsrdf.Activity> getPage(int pageNumber, int size, String userId, org.apache.commons.rdf.api.IRI actorId, String authToken, AvailableCollection collection)  {
    	
//    	int limit = size;
//    	
//		String string = """
//				PREFIX schema: <https://schema.org/> 
//				PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
//				PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
//				PREFIX as: <https://www.w3.org/ns/activitystreams#>
//				
//				SELECT DISTINCT * 
//				FROM <%s>
//				WHERE {
//				  VALUES ?type { as:Create as:Update} 
//				  ?subject rdf:type ?type .
//				  ?subject as:published ?published . 
//				  OPTIONAL { 
//				    ?subject as:name ?name .
//				    ?subject schema:identifier ?identifier .
//				  	#?subject as:summary ?summary .
//				  }
//				} 
//				ORDER BY (?published)
//				LIMIT %d
//				OFFSET %d
//				""";
//		
    	int offset = 0;
    	if(pageNumber > 1) {
    		offset = size * (pageNumber-1);
    	}
		

//		Flux<BindingSet> fluxOfBindingSets;
		Flux<Activity> fluxOfactivities;
		RdfPubClientWebFlux rdfPubClientWebFlux = getRdfPubClientWebFlux(userId);
		switch (collection) {
		case outbox:
						
//			RdfPubClientWebFlux rdfPubClientWebFlux = getRdfPubClientWebFlux(userId);
			fluxOfactivities = rdfPubClientWebFlux.readOutbox(actorId.getIRIString());
			
//	    	String query = string.formatted(actorId.stringValue()+"/" + collection, limit, offset);
//
//    		fluxOfBindingSets = rdfPubClientWebFlux.tupleQueryOutbox(query, authToken);
			break;
		case inbox:
			fluxOfactivities = rdfPubClientWebFlux.readInbox(actorId.getIRIString());
//			rdfPubClientWebFlux = getRdfPubClientWebFlux(userId);
//	    	query = string.formatted(actorId.stringValue()+"/" + collection, limit, offset);
//    		fluxOfBindingSets = rdfPubClientWebFlux.tupleQueryInbox(query, authToken);
			break;
		case asPublic:
			fluxOfactivities = getRdfPubClientWebFluxAnonymous().readPublic();
//			String q = string.formatted(AS.Public, limit, offset);
//			fluxOfBindingSets = getRdfPubClientWebFluxAnonymous().tupleQueryAsPublic(q);
			break;			
		default:
			throw new IllegalArgumentException("Unexpected value: " + collection);
		}

//		Flux<Activity> fluxOfactivities = fluxOfBindingSets
//				.map(bs->new Activity(getString(bs, "identifier"), getString(bs, "type"), getString(bs, "name"), getString(bs, "subject"), formatPublishedDate(bs.getValue("published")))
//				);
		return fluxOfactivities;
    }

//	private String getString(BindingSet bs, String fieldName) {
//		return Optional.ofNullable(bs.getValue(fieldName)).map(Value::stringValue).orElse("null");
//	}
//    
//    private String formatPublishedDate(org.eclipse.rdf4j.model.Value published) {
//    	if(published.isLiteral()) {
//	    	// TODO timezone !!!
//	    	log.trace("published: " + published.stringValue());
//    		LocalDateTime ldt = ((Literal)published).calendarValue().toGregorianCalendar().toZonedDateTime().toLocalDateTime();
//			return ldt.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM));
//    	} else {
//    		return published.stringValue();
//    	}
//    }

//	public Mono<Model> read(IRI subject, String userId, String authToken) { // TODO MOVE to objkect Controller?! is there a read for activity ??
//		RdfPubClientWebFlux rdfPubClientWebFlux = getRdfPubClientWebFlux(userId);
//		return rdfPubClientWebFlux.read(subject);
//	}
	
	private RdfPubClientAnonymousWebFlux getRdfPubClientWebFluxAnonymous() {
		return new RdfPubClientWebFluxAnonymousDefault(rdfPubServerUrl, webClient, activityPubObjectFactory);
	}
	
	private RdfPubClientWebFlux getRdfPubClientWebFlux(String userId) {
		return new RdfPubClientWebFluxDefault(rdfPubServerUrl, webClient, userId, activityPubObjectFactory);
	}
}
