package org.linkedopenactors.code.loaapp.activities;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import lombok.Data;

@Data
public class Activity {
	
	private String identifier;
	private String type;
	private String name;
	private String subject;
	private String published;
	private String subjectAsEncodedUrl;
	
	public Activity(String identifier, String type, String name, String subject) {
		this.identifier = identifier;
		this.type = type;
		this.name = name;
		this.subject = subject;
	}

	public Activity(String identifier, String type, String name, String subject, String published) {
		this(identifier, type, name, subject);
		this.subjectAsEncodedUrl= URLEncoder.encode(subject, StandardCharsets.UTF_8);
		this.published = published;		
	}
}
