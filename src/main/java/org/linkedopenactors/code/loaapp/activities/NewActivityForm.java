package org.linkedopenactors.code.loaapp.activities;

import lombok.Data;

@Data
public class NewActivityForm {
	private String message;
}
