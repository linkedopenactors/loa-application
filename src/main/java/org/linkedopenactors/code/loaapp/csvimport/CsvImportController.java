package org.linkedopenactors.code.loaapp.csvimport;

import java.io.InputStream;
import java.util.List;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.linkedopenactors.loardfpubadapter.LoaRdfPubAdapterWebFlux;
import org.linkedopenactors.rdfpub.client.RdfPubClientWebFlux;
import org.linkedopenactors.rdfpub.client.RdfPubClientWebFluxDefault;
import org.linkedopenactors.rdfpub.domain.commonsrdf.ActivityPubObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.reactive.function.client.WebClient;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.ModelLogger;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@Controller
@Tag(name = "Generic CSV import controller", description = "Gives you the possibility to import csv files.")
@Slf4j
public class CsvImportController {

	@Value("${loa_app.baseNamespace}") 
	private String baseNamespace;
	
	@Autowired
	private WebClient webClient;

	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;
	
	@Value("${loa_app.rdfPubServerUrl}")	
	private String rdfPubServerUrl;//= "https://rdfpub.test.opensourceecology.de/";
	
	@Operation(summary = "TODO")
	@GetMapping("/importCsv")
	public String importCsv(Model model) {
		return "csvimport/importCsv";
	}

	@PostMapping(value = "/importCsvFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String upload(ServerHttpRequest serverHttpRequest, @RequestPart("file") Flux<FilePart> filePartFlux
			, Authentication authentication
			,@RegisteredOAuth2AuthorizedClient("loa-application") 
	 OAuth2AuthorizedClient authorizedClient,
	 Model springModel) {
		
		// TODO provide RdfPubClientWebFlux in some other way  
		RdfPubClientWebFlux rdfPubClien = new RdfPubClientWebFluxDefault(rdfPubServerUrl, webClient, authentication.getName(), activityPubObjectFactory);
		LoaRdfPubAdapterWebFlux loaRdfPubAdapter = new LoaRdfPubAdapterWebFlux(rdfPubClien);
		
		String token = authorizedClient.getAccessToken().getTokenValue();
				
		Flux<ModelAndSubject> models = filePartFlux.flatMap(filePart ->
			filePart.content()
				.map(dataBuffer ->dataBuffer.asInputStream())
				.flatMap(inputStream ->Flux.fromIterable(toModel(filePart.filename(), inputStream, token))));
		
		Flux<IRI> activityIris = 
				models.flatMap(model->loaRdfPubAdapter.createPublication(model.getModel(), token));
		springModel.addAttribute("activityIris", activityIris);
		return "/csvimport/csvImportResult";
	}

	private List<ModelAndSubject> toModel(String filename, InputStream inputStream, String token) {
		
		List<ModelAndSubject> models = new LoaCsvToModel().convert(inputStream, new SimpleNamespace("test", "http://example.com/testNS"));
		models.forEach(m->{
			ModelLogger.debug(log, m.getModel(), "\n######################################################mode");
		});
		return models;
	}
}
