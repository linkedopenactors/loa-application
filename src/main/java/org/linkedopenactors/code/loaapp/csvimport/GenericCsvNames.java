package org.linkedopenactors.code.loaapp.csvimport;

enum GenericCsvNames {
	PublicationLoa_version, 
	PublicationLoa_copyrightNotice, 
	PublicationLoa_creativeWorkStatus,
	PublicationLoa_dateCreated, 
	PublicationLoa_dateModified, 
	PublicationLoa_license, 
	PublicationLoa_keywords,
	PublicationLoa_identifier, 
	PublicationLoa_description, 
	OrgansationLoa_legalName, 
	OrgansationLoa_name,
	OrgansationLoa_url, 
	PlaceLoa_latitude, 
	PlaceLoa_longitude, 
	PostalAddressLoa_postalCode,
	PostalAddressLoa_addressLocality, 
	PostalAddressLoa_addressRegion, 
	PostalAddressLoa_addressCountry,
	PostalAddressLoa_streetAddress, 
	ContactPointLoa_email, 
	ContactPointLoa_name, 
	ContactPointLoa_telephone
}
