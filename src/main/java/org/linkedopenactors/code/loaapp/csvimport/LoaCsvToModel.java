package org.linkedopenactors.code.loaapp.csvimport;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import lombok.extern.slf4j.Slf4j;

/**
 * Abstract base implementation of {@link CsvImporter}.
 */
@Slf4j
class LoaCsvToModel {

	private GenericCsvRecord2PublicationLoaModel genericCsvRecord2PublicationLoaModel = new GenericCsvRecord2PublicationLoaModel();
	private Set<Namespace> additionalNamespaces = new HashSet<>();
	
	/**
	 * Copies the records from the inputFile to the repository 
	 * @param repository The repository into which the records are to be imported.
	 * @param input The inputStream containing the records to import.
	 * @return The number of imported publications.
	 */
	public List<ModelAndSubject> convert(InputStream input, Namespace namespace) {
		log.debug("->" + this.getClass().getSimpleName() + " starting initialization.");
		additionalNamespaces.add(namespace);
		try {
			Reader in = new InputStreamReader(input);
			
			Iterable<CSVRecord> csvRecords = CSVFormat.DEFAULT.withFirstRecordAsHeader().withHeader(getHeaderEnum()).parse(in);
						
			List<List<CSVRecord>> listWithPartialListsOfCsvRecords = ListUtils.partition(IterableUtils.toList(csvRecords), 5000); // If we have a huge csv we get memory problems. Therefor we do partial processing.
			
			List<ModelAndSubject> listOfStatementSets = listWithPartialListsOfCsvRecords.stream().map(partialList-> {
				return convertThing(namespace, partialList);
			})//.collect(Collectors.toList())//;
			
//			stream()
				      .flatMap(Collection::stream)
				      .collect(Collectors.toList());    
			
			return listOfStatementSets;
			
		} catch (Exception e) {
			throw new RuntimeException("error while initial load", e);
		}
	}

	private List<ModelAndSubject> convertThing(Namespace namespace, List<CSVRecord> partialList) {
		log.debug("processing partial list of csvRecords: " + partialList.size());
		List<ModelAndSubject> statements =
				partialList.stream()
				.parallel()
				.map(csvRecord-> convert(csvRecord, namespace))
//				.map(ModelAndSubject::getModel)
//				.flatMap(listContainer -> listContainer.stream())
				.collect(Collectors.toList());
//		Model m = new ModelBuilder().build();
//		m.addAll(statements);
		return statements;
	}

	/**
	 * Converts a csv record into a set of statements (Model).
	 * @param record The csv record to convert
	 * @return A {@link SubjectModelPair} containing the data of the csv record.
	 */
	protected ModelAndSubject convert(CSVRecord record, Namespace namespace) {
		return this.genericCsvRecord2PublicationLoaModel.convert(record, namespace);
	}
	
	/**
	 * See {@link CSVFormat#withHeader(Class)}
	 * @return The enum containing the headers of the csv.
	 */
	protected Class<? extends Enum<?>> getHeaderEnum() {
		return GenericCsvNames.class;
	}

	
//	/**
//	 * Adding the statements to the repository
//	 * @param repository
//	 * @param statements
//	 * @return added statements
//	 */
//	private Set<Statement> toRdfStore(Repository repository, Set<Statement> statements) {
//		log.debug("now adding " + statements.size() + " statements to the repository " + repository);
//		try(RepositoryConnection conMain = repository.getConnection()) {
//			conMain.begin();
//			try {
//				getNamespaces().forEach(ns->conMain.setNamespace(ns.getPrefix(), ns.getName()));
//				conMain.add(statements);				
//				conMain.commit();	
//				log.debug("statements added sucessfully");
//			} catch (Throwable e) {
//				log.error("ROLLBACK: " + e.getMessage(), e);
//				conMain.rollback();
//				return Collections.emptySet();
//			}
//		}
//		return statements;
//	}
	
	
	
//	private IRI toRdfPub(Model model) {
//		return loaRdfPubAdapter.createPublication(model);
//	}
	
	/**
	 * @return The default namesüpaces, that we always expect/use.
	 */
	protected Set<Namespace> getNamespaces() {
		Set<Namespace> namespaces = new HashSet<>();
		namespaces.add(new SimpleNamespace("schema", "https://schema.org/"));
		namespaces.add(new SimpleNamespace("as", "https://www.w3.org/ns/activitystreams#"));
		namespaces.addAll(getAdditionalNamespaces());
		return namespaces;
	}
	
	/**
	 * Gives the implementer the possibility to add specific namespaces to the generated model.
	 * @return additional namespaces
	 */
	protected Set<Namespace> getAdditionalNamespaces() {
		return additionalNamespaces;
	}
}
