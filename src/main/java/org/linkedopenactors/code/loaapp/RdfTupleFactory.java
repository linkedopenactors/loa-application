package org.linkedopenactors.code.loaapp;

public interface RdfTupleFactory {
	RdfTuple create(String nodeId, String parent, boolean objectIsIRI, String subject, String object, String predicate);
}