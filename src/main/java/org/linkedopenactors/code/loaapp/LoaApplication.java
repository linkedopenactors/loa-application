package org.linkedopenactors.code.loaapp;

import java.net.URL;

import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.linkedopenactors.rdfpub.domain.commonsrdf.InstanceProperties;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.commonsrdf.StringToGraphConverter;
import org.linkedopenactors.rdfpub.domain.commonsrdf.SubjectProviderSettings;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.Vocabularies;
import org.linkedopenactors.rdfpub.store.rdf4j.vocab.VocabulariesDefault;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServerOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.security.oauth2.client.web.server.ServerOAuth2AuthorizedClientRepository;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
@ComponentScan(basePackages = {
		"org.linkedopenactors.code.loaapp", 
		"org.linkedopenactors.rdfpub.client", 
		"org.linkedopenactors.rdfpub.domain", 
		"org.linkedopenactors.rdfpub.store.rdf4j.vocab",
		"org.linkedopenactors.rdfpub.store.rdf4j"})
public class LoaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoaApplication.class, args);
	}
	
	@Bean
    WebClient webClient(ServerOAuth2AuthorizedClientRepository authorizedClients
    		, ReactiveClientRegistrationRepository clientRegistrations) {
		ServerOAuth2AuthorizedClientExchangeFilterFunction oauth = new ServerOAuth2AuthorizedClientExchangeFilterFunction(clientRegistrations, authorizedClients);
        oauth.setDefaultClientRegistrationId("loa-application");
        return WebClient.builder()
                .filter(oauth)
                .build();
    }
	
	///////////////////
	// TODO should this be in the client lib ???
    @Bean
    public RDF rdf() {
    	return new RDF4J();
    }
    
	@Bean
	public Vocabularies vocabularies() {
		return new VocabulariesDefault();
	}	
	
    @Bean
    public SubjectProviderSettings subjectProviderSettings() {
    	return new SubjectProviderSettings() {
			
			@Override
			public String getInternalUrnString() {
				// TODO Auto-generated method stub
				return "whyDoWeNeedThisOnClientSide?";
			}
			
			@Override
			public String getInstanceDomainString() {
				// TODO Auto-generated method stub
				return "whyDoWeNeedThisOnClientSide?";
			}
		};
    }
    
    @Bean
    // TODO, how can we encapsulate this away 
    public InstanceProperties instanceProperties() {
    	return new InstanceProperties() {
			
			@Override
			public String getInternalUrnString() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getInstanceDomainString() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getPreferedUsername() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getMavenVersion() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public URL getInstanceDomain() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getIdentifier() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getCommitId() {
				// TODO Auto-generated method stub
				return null;
			}
		};
    }
}
