package org.linkedopenactors.code.loaapp;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.reactive.result.view.Rendering;

import reactor.core.publisher.Mono;

@ControllerAdvice
public class GlobalControllerExceptionHandler {
    @ExceptionHandler(WebClientResponseException.class)
    public  Mono<Rendering>
    defaultErrorHandler(Model theModel, WebClientResponseException e) throws Exception {
        System.out.println("AAAAAAAAAAAAAAAAAA " + e.getMessage());
        
        return Mono.just(Rendering.view("error")
                .modelAttribute("ex", e)
                .build());
    }
}
