package org.linkedopenactors.code.loaapp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.result.view.Rendering;
import org.springframework.web.reactive.result.view.Rendering.Builder;

import reactor.core.publisher.Mono;

@Controller
public class HomeController {

	@Value("${loa_app.rdfPubServerUrl}")	
	private String rdfPubServerUrl;
	
    @GetMapping("/")
    public Mono<Rendering> home(org.springframework.security.core.Authentication authentication) {
        List<String> authorities = authentication.getAuthorities()
                .stream()
                .map(scope -> scope.toString())
                .collect(Collectors.toList());
        
        Builder<?> modelAttribute = Rendering.view("home").modelAttribute("authorities", authorities);
		Rendering build = modelAttribute.build();
		return Mono.just(build);
    }
    
    @GetMapping("/login")
    public Mono<Rendering> login(org.springframework.security.core.Authentication authentication) {
        List<String> authorities = authentication.getAuthorities()
                .stream()
                .map(scope -> scope.toString())
                .collect(Collectors.toList());        
        return Mono.just(
                Rendering.view("home")
                .modelAttribute("authorities", authorities)
                .build());
    }
}
