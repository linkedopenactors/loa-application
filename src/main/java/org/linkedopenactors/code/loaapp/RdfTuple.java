package org.linkedopenactors.code.loaapp;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RdfTuple {
	
	private String nodeId;
	private String parent;
	private String subject;
	private String predicate;
	private String object;
	private String encodedObjectUrl;
	
	/**
	 * True, if the object is an {@link IRI} an should be presented as html link.
	 */
	private boolean objectIsIri; 
	
	/**
	 * True, if the object is an {@link IRI} and it's an rdf-pub ressource.
	 */
	private boolean rdfPubContext;
	
	public String getSubject() {
		return subject;
	}
}
