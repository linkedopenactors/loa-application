package org.linkedopenactors.code.loaapp.objects;

import org.linkedopenactors.code.loaapp.RdfTuple;
import org.linkedopenactors.code.loaapp.RdfTupleFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@Slf4j
public class NodesRestController {

	@Value("${loa_app.rdfPubServerUrl}")
	private String rdfPubServerUrl;
	
	@Autowired
	private WebClient webClient;
		
	@Autowired
	private RdfTupleFactory rdfTupleFactory;

	@GetMapping("/tree/{collection}/{identifier}")
	private Flux<RdfTuple> callRest(@PathVariable String collection, @PathVariable String identifier, Authentication authentication,
			@RegisteredOAuth2AuthorizedClient("loa-application") OAuth2AuthorizedClient authorizedClient) {
		String version = "-1";
		if(rdfPubServerUrl.endsWith("/")) {
			rdfPubServerUrl = rdfPubServerUrl.substring(0, rdfPubServerUrl.length()-1);
		}
		String uri = rdfPubServerUrl + "/actor/"+authentication.getName() + "/" + collection+"/" + version + "/" + identifier;
		log.info("uri: " + uri);
		Flux<RdfTuple> tuples = this.webClient.get()				
				.uri(uri)
				.header(HttpHeaders.ACCEPT, "application/json")
				.retrieve()
				.onStatus(s->s.equals(HttpStatus.NOT_FOUND), response -> Mono.just(new ResponseStatusException(HttpStatus.NOT_FOUND, "Nothing found for identifier: " + identifier + " version: " + version + ", uri:" + uri)))
				.onStatus(HttpStatus::is5xxServerError, response -> Mono.just(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "error!")))
				.bodyToFlux(Node.class)
				.map(this::toRdfTuples)
				.log();
		log.	info("tuples: " + tuples);
		return tuples;
	}
	
	private RdfTuple toRdfTuples(Node node) {
		System.out.println("node: " + node);
		RdfTuple create = rdfTupleFactory.create(node.getNodeId(), node.getParent(), node.isObjectIsIri(), node.getSubject(), node.getObject(), node.getPredicate());
		System.out.println("RdfTuple: " + create);
		return create;
	}
}