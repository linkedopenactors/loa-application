package org.linkedopenactors.code.loaapp.objects;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.util.Values;
import org.linkedopenactors.code.loaapp.RdfTuple;
import org.linkedopenactors.code.loaapp.RdfTupleFactory;
import org.linkedopenactors.code.loaapp.service.RdfPubAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import de.naturzukunft.rdf4j.utils.ModelLogger;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
@Slf4j
public class ObjectController {

	@Autowired
	private RdfPubAdapter rdfPubAdapter;  
	
	@Value("${loa_app.rdfPubServerUrl}")
	private String rdfPubServerUrl;

	@Autowired
	private RdfTupleFactory rdfTupleFactory;

    @GetMapping("/user/outbox/{subject}") 
	public Mono<String> object(@PathVariable String subject, Model model, Authentication authentication,
			@RegisteredOAuth2AuthorizedClient("loa-application") OAuth2AuthorizedClient authorizedClient,
			ServerHttpRequest request) {
    	
    	IRI subjectIri = Values.iri(URLDecoder.decode(subject, StandardCharsets.UTF_8));
    	System.out.println("ObjectController.object -> subjectIri: " + subjectIri);
    	Mono<org.eclipse.rdf4j.model.Model> tuples = rdfPubAdapter.read(authentication, subjectIri);
    	Mono<Boolean> hasIdentifier = tuples.flatMap(this::hasIdentifier);
    	
		Mono<String> page = hasIdentifier.map(hasId -> {
            if (hasId) { 
            	model.addAttribute("identifier", tuples.flatMap(this::getIdentifier));
            	model.addAttribute("collectionName", "outbox");
            	return "objectDetails";
            }
            else {
            	Mono<List<RdfTuple>> tuplesMono = tuples.map(m -> convert(m, subjectIri));
            	Flux<RdfTuple> tuplesFlux = tuplesMono.flatMapIterable(list -> list);
            	model.addAttribute("tuples", tuplesFlux);
            	model.addAttribute("collectionName", "outbox");
            	return "object";
            }
        });

		return page;
    }
    
	private List<RdfTuple> convert(org.eclipse.rdf4j.model.Model model1, IRI subject) {
		ModelLogger.debug(log, model1, "reading " + subject);
			String nodeId = "";
			String parentId = "";
		
		List<RdfTuple> tuples = model1.stream().map(stmt->{
			return rdfTupleFactory.create(nodeId, parentId, stmt.getObject().isIRI(), subject.stringValue(), stmt.getObject().stringValue(), stmt.getPredicate().stringValue());
		}).collect(Collectors.toList());
		return tuples;
	}

	private Mono<Boolean> hasIdentifier(org.eclipse.rdf4j.model.Model model) {
		Set<Statement> stmts = model.filter(null, SCHEMA_ORG.identifier, null);
		boolean hasIdentifier = !stmts.isEmpty();
		return Mono.just(hasIdentifier);
	}
	
	private Mono<String> getIdentifier(org.eclipse.rdf4j.model.Model model) {
		Set<Statement> stmts = model.filter(null, SCHEMA_ORG.identifier, null);
		if(stmts.size()>1) {
			throw new IllegalStateException("Expect one statement, but was: " + stmts.size());
		}
		
		String identifier = stmts.stream()
			.findFirst()
			.map(x->x.getObject())
			.map(org.eclipse.rdf4j.model.Value::stringValue)
			.orElseThrow(()->new IllegalStateException("no identifier! Please first do a check with hasIdentifier(...)"));
		
		return Mono.just(identifier);
	}
}
