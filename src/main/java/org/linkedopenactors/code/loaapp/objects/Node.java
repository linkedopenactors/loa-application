package org.linkedopenactors.code.loaapp.objects;

import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Node {

	private String nodeId;
	private String parent;
    private String subject;
    private String predicate;
    private String object;
    private boolean objectIsIri;

    public Node(String subject, String predicate, String object) {
    	this(null, subject, predicate, object);	
    }
    
    public Node(String parent, String subject, String predicate, String object) {
    	this.nodeId = subject +"_" + predicate;
    	this.parent = parent;
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
    }

	public String getNodeId() {
		return this.nodeId;
	}

	public String getSubjectShort() {
		return Values.iri(subject).getLocalName();
	}
	
	public String getParent() {
		return Optional.ofNullable(parent).orElse("0");
	}

	public String getSubject() {
			return subject;
	}

	public String getPredicate() {
		return Optional.ofNullable(predicate)
				.map(Values::iri)
				.map(IRI::getLocalName)
				.orElse("null");
	}

	public String getObject() {
		return object;
	}

    @Override
	public String toString() {
		return "Node [getNodeId()=" + getNodeId() + ", getParent()=" + getParent() + ", getSubject()=" + getSubject()
				+ ", getPredicate()=" + getPredicate() + ", getObject()=" + getObject() + "]";
	}

	public boolean isObjectIsIri() {
		return objectIsIri;
	}

	public void setObjectIsIri(boolean objectIsIri) {
		this.objectIsIri = objectIsIri;
	}
}
