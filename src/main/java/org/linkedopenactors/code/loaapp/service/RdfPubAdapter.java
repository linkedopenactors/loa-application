package org.linkedopenactors.code.loaapp.service;

import java.io.IOException;
import java.io.StringReader;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.linkedopenactors.rdfpub.client.RdfPubClientWebFlux;
import org.linkedopenactors.rdfpub.client.RdfPubClientWebFluxDefault;
import org.linkedopenactors.rdfpub.domain.commonsrdf.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.commonsrdf.Instance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class RdfPubAdapter {

	// TODO whats is this and why is there no org.linkedopenactors.rdfpub.client.RdfPubClient 
	
	@Autowired
	private WebClient webClient;

	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;

	@Value("${loa_app.rdfPubServerUrl}")
	private String rdfPubServerUrl;

	@Value("${loa_app.rdfPubServerActor}")
	private String rdfPubServerActor;

	public Mono<org.eclipse.rdf4j.model.Model> read(Authentication authentication, IRI subjectIri) { 
		RdfPubClientWebFlux rdfPubClientWebFlux = new RdfPubClientWebFluxDefault(rdfPubServerUrl, webClient, authentication.getName(), activityPubObjectFactory);
		Mono<org.eclipse.rdf4j.model.Model> model = rdfPubClientWebFlux.read(subjectIri);
		return model;
	}
	
	public Mono<org.eclipse.rdf4j.model.Model> getServerProfile() { 
		WebClient client = WebClient.create(rdfPubServerUrl);
		log.info("getServerProfile(): " + rdfPubServerUrl);
		return client.get()
				.uri(Instance.INSTANCE_ACTOR_NAME)
			    .retrieve()
			    .bodyToMono(String.class)
			    .map(httpResponseBody->{
					try {
						return Rio.parse(new StringReader(httpResponseBody), RDFFormat.JSONLD);
					} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
						String message = "Error parsing httpResponseBody. " + e.getMessage();								
						log.trace(message + ": " + httpResponseBody);								
						throw new IllegalStateException(message, e);
					}
				});
	}	

	public Mono<String> dump(Authentication authentication) {
		WebClient client = WebClient.create(rdfPubServerUrl + "/dump");
		log.info("dump(): ");
		return client.get()				
			    .retrieve()
			    .bodyToMono(String.class);
	}	
	
//	public Mono<IRI> getActor(Authentication authentication) {
//		RdfPubClientWebFlux rdfPubClientWebFlux = new RdfPubClientWebFluxDefault(rdfPubServerUrl, webClient, authentication.getName());
//		return rdfPubClientWebFlux.getActorId();
//	}	

//	private IRI getAsRdfPubServerUrl(String extension) {
//		String ext = extension.startsWith("/") ? extension.substring(0, extension.length()-1) : extension;
//		String base = rdfPubServerUrl.endsWith("/") ? rdfPubServerUrl : rdfPubServerUrl +"/";
//		return Values.iri(base + ext);
//	}
}
