package org.linkedopenactors.code.loaapp.admin;

import org.linkedopenactors.code.loaapp.service.RdfPubAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.result.view.Rendering;

import reactor.core.publisher.Mono;

@Controller
public class AdminController {

	@Autowired
	private RdfPubAdapter rdfPubAdapter;
	
	@Value("${loa_app.rdfPubServerUrl}")	
	private String rdfPubServerUrl;//= "https://rdfpub.test.opensourceecology.de/";
	
    @GetMapping("/admin/dump")
    public Mono<Rendering> dump(org.springframework.security.core.Authentication authentication) {
//        List<String> authorities = authentication.getAuthorities()
//                .stream()
//                .map(scope -> scope.toString())
//                .collect(Collectors.toList());
        
        Mono<String> dump = rdfPubAdapter.dump(authentication);
        return Mono.just(Rendering.view("admin/dump").modelAttribute("dump", dump).build());
    }
    
}
