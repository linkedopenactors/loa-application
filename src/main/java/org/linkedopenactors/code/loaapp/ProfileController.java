package org.linkedopenactors.code.loaapp;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.client.RdfPubClientSessionFactory;
import org.linkedopenactors.rdfpub.client.RdfPubClientWebFlux;
import org.linkedopenactors.rdfpub.client.RdfPubClientWebFluxDefault;
import org.linkedopenactors.rdfpub.domain.commonsrdf.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.commonsrdf.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.Instance;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.Vocabularies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.result.view.Rendering;
import org.springframework.web.server.WebSession;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
@Slf4j
public class ProfileController {
	
	@Value("${loa_app.rdfPubServerUrl}")	
	private String rdfPubServerUrl;
		
	@Autowired
	private WebClient webClient;

	@Autowired
	private Vocabularies vocabularies;
	
	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;

	@Autowired
	private RdfTupleFactory rdfTupleFactory;
	
	@Autowired
	private RdfPubClientSessionFactory rdfPubClientSessionFactory;

    @GetMapping("/user/ap-profile")
	public  Mono<Rendering> userApProfile(OAuth2AuthenticationToken authentication, Model model, WebSession session) {
		RdfPubClientWebFlux rdfPubClientWebFlux = 
				new RdfPubClientWebFluxDefault(rdfPubServerUrl, webClient, authentication.getName(), activityPubObjectFactory);
		Mono<Actor> actorProfileMono = rdfPubClientSessionFactory.getSession(rdfPubClientWebFlux, session)
				.flatMap(rdfPubClientSession -> rdfPubClientSession.getCurrentLoggedInActor());
		Flux<RdfTuple> tuplesFlux = actorProfileMono
				.map(this::toRdfTuples)
				.flatMapIterable(list -> list)
				.log();
		return Mono.just(Rendering.view("object")
		        .modelAttribute("tuples", tuplesFlux)
		        .modelAttribute("pageTitle", actorProfileMono.map(Actor::getSubject).map(org.apache.commons.rdf.api.IRI::getIRIString))
		        .build());
	}

    @GetMapping("/user/rdfpub-profile")
    public String rdfpubProfile(OAuth2AuthenticationToken authentication, Model model) {
		RdfPubClientWebFlux rdfPubClientWebFlux = 
				new RdfPubClientWebFluxDefault(rdfPubServerUrl, webClient, authentication.getName(), activityPubObjectFactory);
		
		Mono<Instance> serverProfile = rdfPubClientWebFlux.getServerProfile();
		
		
		Flux<RdfTuple> tuplesFlux = serverProfile
				.map(this::toRdfTuples)
				.flatMapIterable(list -> list)
				.log();
		model.addAttribute("tuples", tuplesFlux);
		model.addAttribute("pageTitle", serverProfile.map(Instance::getSubject)
				.map(org.apache.commons.rdf.api.IRI::getIRIString));
		return "object";
    }

    @GetMapping("/user/profile")
    @PreAuthorize("hasAuthority('SCOPE_profile')")
    public Mono<Rendering> userDetails(OAuth2AuthenticationToken authentication) {
        return Mono.just(Rendering.view("userProfile")
            .modelAttribute("details", authentication.getPrincipal().getAttributes())
            .build());
    }

    private List<RdfTuple> toRdfTuples(Actor actorProfile) {
    	return toRdfTuples(actorProfile.asGraph());
    }
    
    private List<RdfTuple> toRdfTuples(Graph graph) {
        log.debug("-> toRdfTuples()");
    	List<? extends Triple> typeTriples = graph.stream(null, vocabularies.getRdf().type(), null)
    			.collect(Collectors.toList());
    	
    	Set<String> subjects = typeTriples.stream().map(t->t.getSubject().toString()).collect(Collectors.toSet());    	
    	if(subjects.size() > 1) {    		
			throw new IllegalStateException("more than one subject: " + toString(typeTriples));
    	}
    	    	
    	BlankNodeOrIRI subject = typeTriples.stream()
    			.findFirst()
    			.map(stmt->stmt.getSubject())
    			.map(BlankNodeOrIRI.class::cast)
    			.orElseThrow(() -> new IllegalStateException("more than one subject: " + toString(typeTriples)));
    	
		List<RdfTuple> tuples = graph.stream().map(stmt->{
			return rdfTupleFactory.create("", "", false/*stmt.getObject().isIRI()*/, subject.toString(), stmt.getObject().toString(), stmt.getPredicate().getIRIString());
		}).collect(Collectors.toList());
		return tuples;
    }
    
    private String toString(List<? extends Triple> subjects) {    	
    	String asString = subjects.stream()
				.map(triple -> triple.getSubject() + " - " + triple.getPredicate() + " - " + triple.getObject())
                .collect(Collectors.joining("\n"));
    	System.out.println("subjects asString: " + asString);
		return asString;
    }
}
